const timeout = process.env.SLOWMO ? 30000 : 10000;

beforeAll(async () => {
    const testingPage = URL + "doc/"
    await page.goto(testingPage, {waitUntil: 'domcontentloaded'});
});

describe('Test header and title of the page', () => {
    test('Title of the page', async () => {
        const title = await page.title();
        expect(title).toBe('Documentation - The Go Programming Language');
        
    }, timeout);
});
