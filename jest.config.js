module.exports = {
    preset: "jest-puppeteer",
    globals: {
      URL: "https://golang.org/"
    },
    testMatch: [
      "src/test/frontend.test.js"
    ],
    verbose: true
}
